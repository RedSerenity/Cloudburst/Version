using System;
using Cloudburst.Version.Abstractions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;

namespace Cloudburst.Version {
	public static class CloudburstVersionExtensions {
		public static IHostBuilder UseCloudburstVersioning<TServiceVersion>(this IHostBuilder builder) where TServiceVersion : class, IServiceVersion {
			return builder.ConfigureServices((builderContext, services) => {
				services.TryAddSingleton<IServiceVersion, TServiceVersion>();
			});
		}

		public static IApplicationBuilder UseCloudburstVersioning(this IApplicationBuilder app) {
			return app.MapWhen(
				context => IsVersionEndpoint(context),
				builder => builder.UseMiddleware<VersionMiddleware>()
			);
		}

		private static bool IsVersionEndpoint(HttpContext httpContext) {
			return httpContext.Request.Method == "GET" &&
				httpContext.Request.Path.StartsWithSegments("/version", out PathString remaining) &&
						(String.IsNullOrEmpty(remaining) || String.Equals("/", remaining));
		}
	}
}
