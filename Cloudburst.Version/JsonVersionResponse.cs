using Cloudburst.Version.Abstractions;

namespace Cloudburst.Version {
	public class JsonVersionResponse {
		public string Version { get; set; }
		public string FullVersion { get; set; }
		public int Major { get; set; }
		public int Minor { get; set; }
		public int Patch { get; set; }
		public string PreRelease { get; set; }
		public int Build { get; set; }

		public JsonVersionResponse() { }
		public JsonVersionResponse(IServiceVersion serviceVersion) {
			Version = serviceVersion.ToString();
			FullVersion = serviceVersion.FullVersion();
			Major = serviceVersion.Major;
			Minor = serviceVersion.Minor;
			Patch = serviceVersion.Patch;
			PreRelease = serviceVersion.PreRelease;
			Build = serviceVersion.Build;
		}
	}
}
